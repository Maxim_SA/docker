#!/bin/bash

echo "--------- START IMPORTING SQL TO THE DB"
mysql -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE < /app/_resources/install.sql
mysql -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE < /app/_resources/update.sql
echo "--------- DONE IMPORTING SQL TO THE DB"