# Docker

1. Install [docker](https://docs.docker.com/install/)  and [docker compose](https://docs.docker.com/compose/install/)

2. Go to your project source folder , eg (/Projects/sam/dev)
`cd  /Projects/sam/dev`

3. Clone branch you need. Clone the master branch if you use dev branch or clone v3.3 tagged branch for the v3.3 branch(the sam's svn repo)
```bash
git clone https://Maxim_SA@bitbucket.org/Maxim_SA/docker.git docker
```

4. Adjust you hosts file (eg /etc/hosts)
`127.0.0.1	localhost bidpath.local rtbd.bidpath.local`

5. Adjust the site's config (see "Installation Configs" section)

6. If certificates expired, then please generate new according [this instruction](https://leehblue.com/add-self-signed-ssl-google-chrome-ubuntu-16-04/)

7. Go to the docker folder 
`cd  /Projects/sam/dev/docker`

8. Build
`docker-compose build`

9. Start 
`docker-compose up`

## Installation Configs
**core.local.php**

```PHP
'db' => [
    'adapter' => 'MySqli5',                                                                                         
    'server' => 'c_db',                                                                                             
    'port' => null,                                                                                                 
    'database' => 'auctionserver_dev',                                                                              
    'username' => 'root',                                                                                           
    'password' => '1',                                                                                              
    'encoding' => 'latin1', // 'utf8',                                                                              
    'profiling' => false,                                                                                           
    'readonly' => [                                                                                                 
        'enabled' => false,                                                                                         
        'adapter' => 'MySqli5',                                                                                     
        'server' => 'localhost',                                                                                    
        'port' => null,                                                                                             
        'database' => null,                                                                                         
        'username' => null,                                                                                         
        'password' => null,                                                                                         
        'encoding' => 'latin1',                                                                                     
        'profiling' => false,                                                                                       
    ],                                                                                                              
    'mysqlMaxInt' => 2147483647,                                                                                    
],   
'rtb' => [
    'server' => [
        'bindHost' => 'c_rtb',         // RTB_SERVER_BIND
        'bindPort' => 10100,         // RTB_PORT
        'publicHost' => 'rtbd.bidpath.local',       // RTB_SERVER
        'publicPort' => 443,      // RTB_PORT
        'publicPath' => '/',         // RTB_PUBLIC_PATH (SAM-4122)
        'keepAlive' => true,        // RTB_KEEP_ALIVE
        'maxTotalTime' => 172800,   // RTB_MAX_TOTAL_TIME
        'maxIdleTime' => 172800,    // RTB_MAX_IDLE_TIME
        'shouldAuth' => false,      // RTB_AUTH
        'wss' => true,
    ],
],
'app' => [
    'httpHost' => 'bidpath.local',
],
'image' => [                                                                                                        
    'encryptionKey' => '667171514323706b33455835366e', 
],
```

## SQL

To import sql file to the database you might want to use such pattern

```bash
docker exec -i <DOCKER CONTAINER NAME> mysql -u<DB USER> -p<DB PASSWORD> <DATABASE NAME> < path/to/file.sql
```
**Examples:**

Let's say you have 

DOCKER CONTAINER NAME = docker_c_db_1; DB USER = root ; DB PASSWORD = 1

Drop and recreate database

```bash
docker exec -i docker_c_db_1 mysql -uroot -p1 auctionserver_dev <<< "DROP DATABASE auctionserver_dev; CREATE DATABASE auctionserver_dev;"
```

Load fresh sql db

```bash

docker exec -i docker_c_db_1 mysql -uroot -p1 auctionserver_dev < _resources/install.sql
docker exec -i docker_c_db_1 mysql -uroot -p1 auctionserver_dev < _resources/update.sql

```

How to restore some dump.sql.gz (it contains archives sql dump)

```bash
gunzip < dump.sql.gz| docker exec -i docker_c_db_1 mysql -uroot -p1 auctionserver_dev 
```

Dump SQL from a container

```bash
docker exec -i docker_c_db_1 mysqldump -uroot -p1 auctionserver_dev > backup_docker.sql
```

**Important**

If you import a database (eg from the dev server), then don't forget the language folder
`src/resources/language`

## Additional commands

Create `var` folder in the root folder of the project

Install all projet's dependencies vie Composer

```bash
docker exec -it docker_c_php_1 composer i
```

Generate all missing folders

```bash
docker exec -it docker_c_php_1 php bin/install/check.php fs --fix
```

Change permission for upload folder

```bash
docker exec -it docker_c_php_1  chmod -R 0777 uploads
```

## PhpStorm

Don't forget to change debug port to **9009** as there is php-fpm on 9000.

## SSL Certificate

### Openssl Configuration


Make a copy of your main openssl.cnf file to work with:
```bash
cp /etc/ssl/openssl.cnf ~/tmp/ssl/openssl.cnf

```
Now, open the copy of openssl.cnf and add (or uncomment) this line from the [v3_req] section of the file:
```bash
[v3_req]
...
x509_extensions = v3_ca
```

Then, add this section to the end of the [v3_req] section:
```bash
[v3_ca]
...
subjectAltName = @alt_names
[alt_names]
DNS.1 = *.bidpath.local
DNS.2 = bidpath.local
```

### Generate Self-Signed SSL Certificate
```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout bidpath.key -out bidpath.crt -config ~/tmp/ssl/openssl.cnf
```

How to export certificate:

1. Add generated certificate to nginx config
2. Go in your Chrome browser to your local url (eg. bidpath.local)
3. You will see that the https part of the URL is all red-X-ed out
4. Click the lock icon with an X
5. Choose Certificate Information
6. Go to Details tab
7. Click on Export
8. Save as file (eg name _.bidpath.local.crt )

The following command will add the certificate (where YOUR_FILE is your exported file):
```bash
certutil -d sql:$HOME/.pki/nssdb -A -t "P,," -n YOUR_FILE -i YOUR_FILE
```
Example:
```bash
certutil -d sql:$HOME/.pki/nssdb -A -t "P,," -n _.bidpath.local.crt -i _.bidpath.local.crt
````

To see if it actually worked, you can list all of your certificates with this command:
```bash
 certutil -d sql:$HOME/.pki/nssdb -L
```

### Import _.bidpath.local.crt from Chrome Settings->Manage certificates->Authorities

Now, when you go to your site you should see that Google Chrome trusts your self-signed SSL certificate.

### Removing A Certificate


```bash
certutil -D -d sql:$HOME/.pki/nssdb -n -.bidpath.local
```
